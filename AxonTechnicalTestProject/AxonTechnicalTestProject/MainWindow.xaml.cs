﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AxonTechnicalTestProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            txtFrame.KeyUp += TxtFrame_KeyUp;
        }
        
        private void TxtFrame_KeyUp(object sender, KeyEventArgs e)
        {
            RemoveUndesiredCharacters();

            txtFrame.SelectionStart = txtFrame.Text.Length;
            txtFrame.SelectionLength = 0;
        }

        private void BtnFrameSend_Click(object sender, RoutedEventArgs e)
        {
            RemoveUndesiredCharacters();
            ClearLabels();
            if (txtFrame.Text.Length > 2) DataCalculating();
        }

        void DataCalculating()
        {
            string actualOctate = "";
            string binaryValue = "";
            string type = "";

            //Verifies if is there text
            if(txtFrame.Text == "")
            {
                return;
            }

            //Verifies if there's a octates number complete
            if (!(txtFrame.Text.Length % 2 == 0))
            {
                MessageBox.Show("La trama está incompleta, esto indica que existe un octeto incompleto. Recuerde que el número de caracteres debe ser par", "Trama incompleta", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            //Verifies initial character
            try
            {
                actualOctate = GetInterestedOctate(1);
            }
            catch
            {
                MessageBox.Show("Existe un problema con el octeto del inicio, por favor, revíselos", "Octetos del inicio corrupto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if(!(actualOctate == "68"))
            {
                MessageBox.Show("El octeto inicial no corresponde con el número 68 en hexadecimal, compruebe el octeto inicial", "Octeto inicial corrupto", MessageBoxButton.OK, MessageBoxImage.Error);
                lblStar.Content = "Valor Inicio = Incorrecto";
                return;
            }
            else
            {
                lblStar.Content = "Valor Inicio = Correcto";
            }

            //Verifies length
            try
            {
                actualOctate = GetInterestedOctate(2);
            }
            catch
            {
                MessageBox.Show("Existe un problema con el octeto del largo, por favor, revíselos", "Octetos de largo corrupto", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            if(!(StringToDecimal(actualOctate) == ((txtFrame.Text.Length / 2) - 2)))
            {
                MessageBox.Show("Largo de la cadena no coincide con el segundo caracter", "El largo está errado", MessageBoxButton.OK, MessageBoxImage.Error);
                lblLength.Content = "Valor Longitud = Incorrecto";
                return;
            }
            else
            {
                lblLength.Content = "Valor Longitud = Correcto";
            }

            //Verifies frame type
            // Type I
            try
            {
                if (StringToBinary(GetInterestedOctate(3))[StringToBinary(GetInterestedOctate(3)).Length - 1] == '0' && StringToBinary(GetInterestedOctate(5))[StringToBinary(GetInterestedOctate(5)).Length - 1] == '0')
                {
                    type = "I";
                    lblFrameType.Content = "Trama Tipo = I";
                }
                else
                {
                    // Type S
                    if (StringToDecimal(GetInterestedOctate(3)) == 1 && StringToDecimal(GetInterestedOctate(4)) == 0 && StringToBinary(GetInterestedOctate(5))[StringToBinary(GetInterestedOctate(5)).Length - 1] == '0')
                    {
                        type = "S";
                        lblFrameType.Content = "Trama Tipo = S";
                        lblObjectType.Content = "Tipo de Objetos = --";
                        lblTransmissionCause.Content = "Causa Transmisión = --";
                    }
                    else
                    {
                        //Type U
                        if (StringToBinary(GetInterestedOctate(3))[StringToBinary(GetInterestedOctate(3)).Length - 1] == '1' && StringToBinary(GetInterestedOctate(3))[StringToBinary(GetInterestedOctate(3)).Length - 2] == '1' && StringToDecimal(GetInterestedOctate(4)) == 0 && StringToDecimal(GetInterestedOctate(5)) == 0 && StringToDecimal(GetInterestedOctate(6)) == 0)
                        {
                            type = "U";
                            lblFrameType.Content = "Trama Tipo = U";
                            lblSentSequence.Content = "Num. Seq. Enviados = --";
                            lblTakenSequence.Content = "Num. Seq. Recibidos = --";
                            lblObjectType.Content = "Tipo de Objetos = --";
                            lblTransmissionCause.Content = "Causa Transmisión = --";
                        }
                    }
                }
            }
            catch
            {
                MessageBox.Show("Existe un problema con los octetos de control, por favor, revíselos", "Octetos de control corruptos", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
                        
            // All data of type I
            if(type == "I")
            {
                //Verifies sent and received sequence for I
                binaryValue = StringToBinary(GetInterestedOctate(3));

                while (binaryValue.Length < 8)
                {
                    binaryValue = "0" + binaryValue;
                }

                int sentSequence = BinaryToInteger(StringToBinary(GetInterestedOctate(4)) + binaryValue.Remove(binaryValue.Length - 1, 1));
                lblSentSequence.Content = "Num. Seq. Enviados = " + sentSequence.ToString();

                //Verifies received sequence for I
                binaryValue = StringToBinary(GetInterestedOctate(5));

                while (binaryValue.Length < 8)
                {
                    binaryValue = "0" + binaryValue;
                }

                int receivedSequence = BinaryToInteger(StringToBinary(GetInterestedOctate(6)) + binaryValue.Remove(binaryValue.Length - 1, 1));
                lblTakenSequence.Content = "Num. Seq. Recibidos = " + receivedSequence.ToString();

                //Verifies Objects Type
                int objectType = StringToDecimal(GetInterestedOctate(7));
                lblObjectType.Content = "Tipo de Objetos = " + objectType.ToString();

                //Verifies transmission cause
                binaryValue = StringToBinary(GetInterestedOctate(9));

                while (binaryValue.Length < 8)
                {
                    binaryValue = "0" + binaryValue;
                }

                binaryValue = binaryValue.Remove(0, 2);

                int transmissionCause = BinaryToInteger(binaryValue);
                lblTransmissionCause.Content = "Causa Transmisión = " + transmissionCause.ToString();
            }

            // All data of type S
            if (type == "S")
            {
                //Verifies sent and received sequence for S
                binaryValue = StringToBinary(GetInterestedOctate(3));

                while (binaryValue.Length < 8)
                {
                    binaryValue = "0" + binaryValue;
                }

                int sentSequence = BinaryToInteger(StringToBinary(GetInterestedOctate(4)) + binaryValue.Remove(binaryValue.Length - 1, 1));
                lblSentSequence.Content = "Num. Seq. Enviados = " + sentSequence.ToString();

                //Verifies received sequence for S
                binaryValue = StringToBinary(GetInterestedOctate(5));

                while (binaryValue.Length < 8)
                {
                    binaryValue = "0" + binaryValue;
                }

                int receivedSequence = BinaryToInteger(StringToBinary(GetInterestedOctate(6)) + binaryValue.Remove(binaryValue.Length - 1, 1));
                lblTakenSequence.Content = "Num. Seq. Recibidos = " + receivedSequence.ToString();
            }
            
        }



        void RemoveUndesiredCharacters()
        {
            if (txtFrame.Text.Length > 0)
            {
                for (int i = 0; i < txtFrame.Text.Length; i++)
                {
                    if (!((txtFrame.Text[i] >= 65 && txtFrame.Text[i] <= 70) || (txtFrame.Text[i] >= 48 && txtFrame.Text[i] <= 57) || (txtFrame.Text[i] >= 97 && txtFrame.Text[i] <= 102)))
                    {
                        txtFrame.Text = txtFrame.Text.Remove(i, 1);
                    }
                }
            }
        }

        string GetInterestedOctate(int index)
        {
            string octate = "";

            octate = txtFrame.Text.Substring((index - 1) * 2, 2);
            
            return octate;
        }

        int StringToDecimal(string octate)
        {
            return Convert.ToInt32(octate, 16);
        }

        string StringToBinary(string octate)
        {
            return Convert.ToString(StringToDecimal(octate), 2);
        }

        int BinaryToInteger(string binaryValue)
        {
            return Convert.ToInt32(binaryValue, 2);
        }

        void ClearLabels()
        {
            lblStar.Content = "Valor Inicio = ";
            lblLength.Content = "Valor Longitud = ";
            lblFrameType.Content = "Trama Tipo = ";
            lblSentSequence.Content = "Num. Seq. Enviados = ";
            lblTakenSequence.Content = "Num. Seq. Recibidos = ";
            lblObjectType.Content = "Tipo de Objetos =";
            lblTransmissionCause.Content = "Causa Transmisión = ";
        }
    }
}
